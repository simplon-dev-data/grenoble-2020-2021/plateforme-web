# Plateforme web

Le but de cette activité est de créer **une base de code python** pour créer une plateforme web pour gérer un tableau de bord de données.

## Activité de recherche d'informations

- Lister différents frameworks web Python

- Lister des sites/plateformes web qui utilisent Django

- Quelles sont les spécificités de Django ?

- Qu'est-ce qu'une vue (view) en Django ?

- Qu'est-ce qu'un modèle (model) en Django ?

- Qu'est-ce qu'un gabarit (template) en Django ?

## Activité pratique : partie 1

### Objectif

Dans cette partie, on va créer la page principale de la plateforme.

### Etapes

1 - Créer un dossier pour cette activité

2 - Créer un environnement virtuel et y installer django

3 - Vérifier la version de Django (3.1)

4 - Créer un projet Django nommé "projet_django" et vérifier le contenu

5 - Lancer le serveur de développement et vérifier son fonctionnement

6 - Créer une application nommée "dataviz" et vérifier sa création. Ajouter l'application dans le fichier `settings.py` dans `INSTALLED_APPS` 

7 - Dans cette application, créer une vue minimaliste qui affiche le texte suivant "Ceci est la page principale de la plateforme" en s'appuyant sur ce lien https://docs.djangoproject.com/fr/3.1/intro/tutorial01/. Vérifier son affichage avec le serveur de développement.

8 - Dans l'application "dataviz", créer un dossier "templates" puis dans ce dossier créer un dossier "dataviz". Dans ce dossier, créer un fichier "base.html" sur ce modèle :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>{% block title %}Plateforme de visualisation de données{% endblock %}</title>
</head>

<body>
    <div id="content">
        {% block content %}{% endblock %}
    </div>
</body>
</html>
```

Dans ce même dossier, créer un fichier `home.html` sur ce modèle :

```html
{% extends "dataviz/base.html" %}

{% block title %}Homepage - Plateforme de visualisation de données{% endblock %}

{% block content %}
<h1>
    Page principale de la plateforme de visualisation de données
</h1>
{% endblock %}
```

9 - Modifier le code de la vue minimaliste pour afficher le template `home.html`. Vérifier son affichage avec le serveur de développement.

10 - Passer le titre de la page en bleu grâce à un fichier CSS (https://docs.djangoproject.com/fr/3.1/intro/tutorial06/#customize-your-app-s-look-and-feel)

11 - Dans la vue principale, créer une liste d'élèments et afficher ces données sous forme de liste non numérotée dans le template `home.html` en la passant au contexte de la fonction render()

### Ressources

- https://docs.djangoproject.com/fr/3.1/intro/tutorial01/

- https://docs.djangoproject.com/fr/3.1/intro/tutorial03/#a-shortcut-render

## Activité pratique : partie 2

### Objectif

Dans cette partie, on va mettre en place l'authentification.

### Etapes

1 - Effectuer les migrations latentes à l'aide de la commande : `python manage.py migrate`

2 - Créer un super-utilisateur à l'aide de la commande : `python manage.py createsuperuser`

3 - Lancer le serveur de développement et accéder à l'url http://127.0.0.1:8000/admin/

4 - Dans l'espace d'administration, créer un nouvel utilisateur.

5 - Dans le fichier `urls.py` du projet, ajouter `path('accounts/', include('django.contrib.auth.urls')),` dans la liste `urlpatterns`

6 - A la racine du projet, créer un dossier "templates" puis dans ce dossier créer un dossier "registration". Dans ce dossier, créer un fichier "login.html" sur ce modèle :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Login</title>
</head>

<body>
    <div id="content">
    {% if form.errors %}
        <p>Your username and password didn't match. Please try again.</p>
    {% endif %}

    {% if next %}
        {% if user.is_authenticated %}
        <p>Your account doesn't have access to this page. To proceed,
        please login with an account that has access.</p>
        {% else %}
        <p>Please login to see this page.</p>
        {% endif %}
  {% endif %}

  <form method="post" action="{% url 'login' %}">
    {% csrf_token %}
    <table>
      <tr>
        <td>{{ form.username.label_tag }}</td>
        <td>{{ form.username }}</td>
      </tr>
      <tr>
        <td>{{ form.password.label_tag }}</td>
        <td>{{ form.password }}</td>
      </tr>
    </table>
    <input type="submit" value="login" />
    <input type="hidden" name="next" value="{{ next }}" />
  </form>

  {# Assumes you setup the password_reset view in your URLconf #}
  <p><a href="{% url 'password_reset' %}">Lost password?</a></p>
    </div>
</body>
</html>
```

Dans ce même dossier, créer un fichier `logged_out.html` sur ce modèle :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Logout</title>
</head>

<body>
    <p>Logout</p>
</body>
```

7 - Sur la vue créée à la partie précédente, ajouter une restriction d'accès :

```python
from django.contrib.auth.decorators import login_required

...

@login_required
def home(request):
    return render(request, 'dataviz/home.html')
```

8 - Lancer le serveur de développement et vérifier que la page principale n'est pas en accès libre. Vérifier le bon fonctionnement de la page de connexion (http://127.0.0.1:8000/accounts/login) et de de connexion (http://127.0.0.1:8000/accounts/logout)


### Ressources

- https://docs.djangoproject.com/fr/3.1/topics/auth/default/#the-login-required-decorator
