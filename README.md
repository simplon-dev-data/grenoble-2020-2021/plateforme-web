# Plateforme web

Le but de cette activité est de créer **une base de code python** pour créer une plateforme web pour gérer un tableau de bord de données.


## Ressources

- Librairie Bootstrap : https://getbootstrap.com/

- Template Dashboard Bootstrap : https://getbootstrap.com/docs/5.0/examples/dashboard/

- Librairie de visualisation de données ChartJS : https://www.chartjs.org/docs/latest/ 

- Librairie de cartographie Leaflet : https://leafletjs.com/
