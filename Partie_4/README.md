# Partie 4 : déploiement sur Heroku

Dans cette partie de l'activité, nous allons enfin mettre à disposition notre
application sur le web ! Ce procédé s'appelle le _déploiement_.

Nous allons utiliser la plateforme d'hébergement cloud Heroku car elle est
très simple d'utilisation et bien appropriée au déploiement d'applications web.

Documentation :

- https://devcenter.heroku.com/articles/git
- https://devcenter.heroku.com/categories/working-with-django
- https://devcenter.heroku.com/articles/django-app-configuration
- https://devcenter.heroku.com/articles/django-assets

## Pré-requis

1. Créer un compte utilisateur Heroku gratuit : https://www.heroku.com
2. Installer le programme Heroku en ligne de commande : https://devcenter.heroku.com/articles/heroku-cli#download-and-install

## Installation des paquets nécessaires

Afin de pouvoir déployer une application Django en mode _production_,
il est nécessaire d'ajouter les paquets suivants aux dépendances (`Pipfile`) :

- `gunicorn` : permet de lancer Django dans un vrai serveur web de production
- `honcho` : permet de simuler le comportement de Heroku en local
- `whitenoise` : permet de gérer automatiquement les fichiers statiques du projet Django

## Modification des paramètres

Il est nécessaire d'effectuer quelques ajustements dans le fichier `settings.py` :

- Désactiver le mode debug :

```python
DEBUG = False
```

- Autoriser tous les noms de domaines :

```python
ALLOWED_HOSTS = ['*']
```

- Ajouter le support de `whitenoise` (**attention** : bien le mettre en premier dans la liste):

```python
MIDDLEWARE = [
    'whitenoise.middleware.WhiteNoiseMiddleware',
    ...
]
```

- Configurer les fichiers statiques :

```python
STATIC_ROOT = BASE_DIR / 'staticfiles'
STATIC_URL = '/static/'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
```

- (optionnel dans notre cas) Rendre dynamique la clé secrète :

```python
import os

...

SECRET_KEY = os.environ['SECRET_KEY']
```

## Créer le fichier de configuration pour Heroku

Afin que Heroku sache comment lancer notre application web Django,
il est nécessaire de spécifier le programme et les arguments à exécuter.

Pour cela, il suffit de créer un fichier `Procfile` à la racine du projet
avec le contenu suivant :

```
web: gunicorn plateforme.wsgi
```

## Tester en local

Notre application est enfin prête à être déployée sur Heroku !
Avant de lancer le processus, il est de bonne pratique de pouvoir simuler
le déploiement en local. Grâce aux paquets `whitenoise` et `honcho`, nous
pouvons le faire en local :

1. Collecter les fichiers statiques :

```bash
python manage.py collectstatic
```

2. Lancer l'application :

```bash
honcho start
```

Si tout est bien configuré, l'application doit tourner en local en mode
production sur http://0.0.0.0:5000

Lorsque les ajustements sont terminés, **commiter dans Git** les changements.

## Déployer avec Heroku

Pour déployer une application sur Heroku, le moyen le plus simple est d'utiliser Git.
Comme nous avons apprit à versionner nos applications avec Git, c'est le compagnon parfait !

1. Créer une nouvelle application Heroku dans le dossier du projet :

```bash
heroku create
```

2. Vérifier que le remote Git `heroku` est bien configuré :

```bash
git remote -v
```

3. Déployer l'application Django sur Heroku avec Git :

```bash
git push heroku master
```

4. Attendre la fin du processus de déploiement puis consulter l'adresse
   web indiquée (de la forme https://xxx.herokuapp.com)

5. (optionnel) Si la clé secrète est rendu dynamique, il faut la configurer sur Heroku :

```bash
heroku config:set SECRET_KEY=$(python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())')
```

À chaque nouveau commit dans le dépôt Git de l'application, vous pouvez
déployer une nouvelle version sur Heroku simplement en poussant vos commits !

Si vous ne souhaitez pas/plus garder votre application en ligne, vous pouvez
la détruire avec la commande suivante (attention c'est irreversible !) :

```bash
heroku apps:destroy
```
