## Activité pratique

### Objectif

Dans cette partie, on va afficher un tableau de bord avec Django

### Etapes

1 - Créer un projet Django nommé "projet_dashboard" et une application nommée "dataviz"

2 - Créer un dossier de fichiers `templates` et `static`

3 - Copier les fichiers `base.html` et `home.html` dans le dossier `templates`

4 - Copier les fichiers `dashboard.css` et `dashboard.js` dans le dossier `static`

5 - Créer une vue avec un dictionnaire de contexte avec les clés "data"  et "label"

6 - Vérifier l'affichage du grahique

7 - Modifier le code du tableau pour afficher les valeurs dans le tableau

8 - Créer le différentes vues des liens de la plateforme