## Activité pratique

### Objectif

Dans cette partie, on va insérer les données d'un fichier CSV dans la base de données gérée par Django et on va visualiser les données.

Le fichier de données à visualiser est le fichier de résultat des élections : cantonales-2011-1er-tour.csv

### Etapes

1 - Reprendre le code du tableau de bord Django de la partie 2

2 - Mettre à jour le dossier de templates. Ajouter dans le dossier `templates\dataviz` de l'application `dataviz` les fichiers `home.html`, `import_csv.html` et `visu.html` présents dans ce dossier

3 - Dans le fichier `models.py`, créer une classe `Election` :

```python
from django.db import models

class Election(models.Model):
    bureau = models.CharField(max_length=200)
    votants = models.IntegerField()

    def __str__(self):
        return f"Données du bureau {self.bureau} - Votants {str(self.votants)}"
```

4 - Créer et effectuer la migration (`makemigrations` et `migrate`)

5 - Ajouter le lien vers le model dans le fichier `admin.py`

```python
from django.contrib import admin
from .models import Election

admin.site.register(Election)
```

6 - Mettre à jour le fichier `views.py` de l'application `dataviz` :

```python
import csv
from django.shortcuts import render
from .models import Election

def home(request):
    return render(request, 'dataviz/home.html')

def import_csv(request):
    if request.method == "GET":
        return render(request, 'dataviz/import_csv.html')
    file_csv = request.FILES['file']
    data = [row for row in csv.reader(file_csv.read().decode('UTF-8').splitlines())]
    for row in data[1:]:
        Election.objects.create(
                bureau=row[0],
                votants=row[1],
                )
    return render(request, 'dataviz/home.html')

def visu(request):
    context = {
        'label' : [elt.bureau for elt in Election.objects.all()],
        'data' : [elt.votants for elt in Election.objects.all()],
    }
    return render(request, 'dataviz/visu.html', context=context)
```

7 - Mettre à jour le fichier `urls.py` avec les vues `home`, `import_csv` et `visu`

8 - Mettre à jour les liens vers les pages `home`, `import_csv` et `visu` dans le templates `base.html`

9 - Lancer le serveur de développement et importer le fichier "cantonales-2011-1er-tour.csv" sur la page d'importation des fichiers CSV. Vérifier que la visualisation fonctionne.